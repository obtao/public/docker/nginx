# NGINX

Image NGINX utilisée par les applications Symfony de la démo Kubernetes. 

## Principe

Les socles Docker ont pour but de faciliter le développement des applications sous docker et leur déploiement sous Kubernetes.
Cette image `nginx` existe pour être utilisée avec l'image `php-fpm`.

Cette image n'a  pas pour but d'être partagée ou utilisée par d'autres, mais peut servir d'exemple pour créer votre propre socle. 
Elle intègre des logs en JSON qui seront récupérés via Fluentd. 

Le *user* de l'image docker est `nginx` pour éviter les images lancées en `root`.


## CI

L'image est buildée par Gitlab-CI : 
* `latest` est buildée lors d'un push sur *master*
* les `tags` sont buildés lorsqu'un tag est poussé sur git.

A terme cette image pourrait être buildée et testée sur des patterns de branches avant d'être mergée dans master.


