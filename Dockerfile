FROM nginx:1.17-alpine

ADD default.conf.dist /tmp/default.conf.dist
COPY nginx.conf /etc/nginx/nginx.conf
COPY mod /etc/nginx/conf.d/mod

COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint
RUN chown 101:101 /etc/nginx/conf.d/default.conf

ENTRYPOINT ["docker-entrypoint"]

CMD ["nginx", "-g", "daemon off;"]
