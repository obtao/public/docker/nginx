#!/bin/sh
set -e

# Gestion des spécificités docker/kubernetes
if [ "$NAMESERVER" == "" ]; then
    export NAMESERVER=`cat /etc/resolv.conf | grep "nameserver" | awk '{print $2}' | tr '\n' ' '`
fi

echo "Copying nginx config"
envsubst '$NAMESERVER' < /tmp/default.conf.dist > /etc/nginx/conf.d/default.conf

exec "$@"
